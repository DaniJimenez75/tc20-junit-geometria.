package Team1.TC20_JUnitGeometria;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import dto.Geometria;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testAreaCuadrado()
    {
        double resultado = Geometria.areacuadrado(3);
        double esperado = 9;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testAreaCirculo()
    {
        double resultado = Geometria.areaCirculo(4);
        double esperado = 50.27;
        assertEquals(esperado, resultado,1);
    }
    @Test
    public void testAreaTriangulo()
    {
        double resultado = Geometria.areatriangulo(5, 4);
        double esperado = 10;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testAreaRectangulo()
    {
        double resultado = Geometria.arearectangulo(4, 4);
        double esperado = 16;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testAreaPentagono()
    {
        double resultado = Geometria.areapentagono(6, 5);
        double esperado = 15;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testAreaRombo()
    {
        double resultado = Geometria.arearombo(6, 6);
        double esperado = 18;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testAreaRomboide()
    {
        double resultado = Geometria.arearomboide(10, 10);
        double esperado = 100;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testAreaTrapecio()
    {
        double resultado = Geometria.areatrapecio(5, 5, 2);
        double esperado = 10;
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura1()
    {
        String resultado = Geometria.figura(1);
        String esperado = "cuadrado";
        assertEquals(esperado, resultado);
    }
    @Test
    public void testFigura2()
    {
        String resultado = Geometria.figura(2);
        String esperado = "Circulo";
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura3()
    {
        String resultado = Geometria.figura(3);
        String esperado = "Triangulo";
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura4()
    {
        String resultado = Geometria.figura(4);
        String esperado = "Rectangulo";
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura5()
    {
        String resultado = Geometria.figura(5);
        String esperado = "Pentagono";
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura6()
    {
        String resultado = Geometria.figura(6);
        String esperado = "Rombo";
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura7()
    {
        String resultado = Geometria.figura(7);
        String esperado = "Romboide";
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testFigura8()
    {
        String resultado = Geometria.figura(8);
        String esperado = "Trapecio";
        assertEquals(esperado, resultado);
    }
    @Test
    public void testFigura9()
    {
        String resultado = Geometria.figura(9);
        String esperado = "Default";
        assertEquals(esperado, resultado);
    }
    
    
    @Test
    public void testSetId() {
    	Geometria g1 = new Geometria();
    	g1.setId(10);
    	int resultado = g1.getId();   	
    	int esperado = 10;
    	assertEquals(esperado, resultado);
    }
    

    @Test
    public void testSetnom() {
    	Geometria g1 = new Geometria();
    	g1.setNom("cuadrado");
    	String resultado = g1.getNom();
    	String esperado = "cuadrado";
    	assertEquals(esperado, resultado);
    }
    
    @Test
    public void testGetId() {
    	Geometria g1 = new Geometria();
    	int resultado = g1.getId();   	
    	int esperado = 9;
    	assertEquals(esperado, resultado);
    }
    @Test
    public void testGetnom() {
    	Geometria g1 = new Geometria();
    	String resultado = g1.getNom();
    	String esperado = "Default";
    	assertEquals(esperado, resultado);
    }
    @Test
    public void testGetArea() {
    	Geometria g1 = new Geometria(1);
    	double resultado = g1.areacuadrado(5);
    	g1.setArea(resultado);
    	double esperado = g1.getArea();
    	assertEquals(esperado, resultado);
    }
    @Test
    public void testSetArea() {
        Geometria g1 = new Geometria();
        g1.setArea(2.0);
        double resultado = g1.getArea();
        double esperado = 2.0;
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testSettoString() {
        Geometria g1 = new Geometria();
        g1.setArea(2.5);
        double area = g1.getArea();
        String resultado = g1.toString();
        String esperado = "Geometria [id=" + 9 + ", nom=" + "Default" + ", area=" + area + "]";
        assertEquals(esperado, resultado);
    }
    
    
   
    
    
    
    
}
